var mysql = require("mysql");
var fs = require("fs");

const NewsDAO = require('./newsDAO.js');

// Johandev MySQL server
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "johandev.no",
  user: "newsadmin",
  password: "ciBJ8o0IxgT8YGJg",
  database: "news_mock",
  debug: false,
  multipleStatements: true
});

var newsDAO = new NewsDAO(pool);

afterAll(() => {
  pool.end((err) => {
    if(err){
      console.log(err);
    }
    else{
      console.log("Disconnected from database\nTest completed");
    }
  });
});

beforeAll(done => {
    runSQLfile("../sql/createTables_test.sql", pool, () => {
        runSQLfile("../sql/insert_test.sql", pool, done);
    });
});

test("getComments", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(1);
    expect(data[0].body).toBe("Fake news!!!");

    done();
  }

  newsDAO.getComments(1, callback);
});




test("saveComment", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.affectedRows).toBe(1);
    expect(data.serverStatus).toBe(2);
    expect(data.warningCount).toBe(0);

    newsDAO.getComments(2, (status, data) => {
      expect(data[0].body).toBe("12390asdklcfutcfut123bvdfhsfgh0990234sdfvb");
      expect(data[0].signature).toBe("09ufeghpioi12350a9dcvadckkladsfg0pnplk");
      done();
    });
    //expect(data[0].description).toBe("Sport");

  }
  
  let data = {body: "12390asdklcfutcfut123bvdfhsfgh0990234sdfvb", signature: "09ufeghpioi12350a9dcvadckkladsfg0pnplk", news_id: 2};
  newsDAO.saveComment(data, callback);
});

var runSQLfile = ((filename, pool, done) => {
  console.log("runsqlfile: reading file " + filename);
  let sql = fs.readFileSync(filename, "utf8");
  pool.getConnection((err, connection) => {
    if (err || connection === undefined) {
      console.log("runsqlfile: error connecting");
      done();
    } else {
      console.log("runsqlfile: connected");
      connection.query(sql, (err, rows) => {
        connection.release();
        if (err) {
          console.log(err);
          done();
        } else {
          console.log("runsqlfile: run ok");
          done();
        }
      });
    }
  });
});
