// @flow

const WebSocket = require('ws');
const mysql = require("mysql");
const NewsDAO = require('./newsDAO.js');

const wss = new WebSocket.Server({ port: 3002 });



console.log("Started commentWS server");

let pool = mysql.createPool({
  connectionLimit: 2,
  host: "johandev.no",
  user: "newsadmin",
  password: "ciBJ8o0IxgT8YGJg",
  database: "news_dev",
  debug: false
});

let newsDAO = new NewsDAO(pool);
 

let connections = [];

//let connectionMap = {};

 
wss.on('connection', function connection(ws) {
    
    console.log("Got new connection");
    ws.on('close', function close(){
        console.log("Closed connection");
        let conTemp = [];
        connections.forEach((conArt) => {
            if(conArt.socket != ws){
                conTemp.push(conArt);
            }
        });

        connections = conTemp;
    });

    ws.on('message', function incoming(message) {
        console.log("Recieved message: ", message);
        let msgJson = {};
        let parsed = true;
        try{
            msgJson = JSON.parse(message);
        }
        catch(e){
            console.log("Incorrect data recieved: " + e);
            parsed = false;
        }

        if(parsed){
            if(msgJson.validator == 'safgn90aasdgfvvv!!z3c5xjy08'){
                if(msgJson.operation == 'load'){

                    let articleConnection = {id: msgJson.id, socket: ws};
                    connections.push(articleConnection);
                    //connectionMap[ws] = articleConnection;

                    newsDAO.getComments(msgJson.id, (status, data) => {
                        ws.send(JSON.stringify(data));
                    });
                }
                if(msgJson.operation == 'save'){
                    let data = {body: msgJson.data.comment, signature: msgJson.data.author, news_id: msgJson.id}
                    newsDAO.saveComment(data, (status, data) => {
                        if(status == 200){
                            console.log("Saved comment");
                            sendComments(msgJson.id);
                        }
                        else{
                            console.log("Could not save comment: " + status);
                        }
                        //Send update to clients currently reading the same article
                    });
                }
            }
        }  
    });
});

// Broadcast to all clients reading the article with given id
function sendComments(id: number){
    newsDAO.getComments(id, (status, data) => {
        //console.log('sendComments: ', id, data);

        // for(let key in connectionMap){
        //     let articleConnection = connectionMap[key];
        //     if(articleConnection.id == id && articleConnection.socket.readyState === WebSocket.OPEN){
        //         articleConnection.socket.send(JSON.stringify(data));
        //     }
        // }
    
        
        connections.forEach((articleConnection) => {
            if(articleConnection.id == id && articleConnection.socket.readyState === WebSocket.OPEN){
                articleConnection.socket.send(JSON.stringify(data));
            }
        });
        
    });
}

// Broadcast to all.
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
};