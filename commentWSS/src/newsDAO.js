// @flow

const Db = require("./db.js");

module.exports = class NewsDAO extends Db{
  getComments(news_id: number, callback: any){
    super.query("SELECT * FROM comments WHERE news_id = ?;", [news_id], callback);
  }

  saveComment(data: Object, callback: any){
    console.log(data.body, data.signature, data.news_id);
    super.query("INSERT INTO comments (id, body, signature, upload_time, news_id) VALUES (DEFAULT, ?, ?, DEFAULT, ?);",
    [data.body, data.signature, data.news_id],
    callback);
  }
};
