//@ flow

var mysql = require("mysql");

module.exports = class Db{
  constructor(pool: Pool){
    this.pool = pool;
  }

  query(sql, params, callback){
    this.pool.getConnection((err, connection) => {
      //console.log("Connecting to database");
      if(err){
        //console.log("Error with database connection");
        callback(500, {error: "Error with database connection"});
      }
      else{
        //console.log("Running sql query: " + sql);
        connection.query(sql, params, (err, rows) => {
          connection.release();
          if(err){
            //console.log(err);
            callback(500, {error: "Error running query"});
          }
          else{
            //console.log("returning rows");
            callback(200, rows);
          }
        });
      }
    });
  }
};
