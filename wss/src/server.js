// @flow

const WebSocket = require('ws');
const mysql = require("mysql");

const wss = new WebSocket.Server({ port: 3001 });

console.log("Stared WS server");
 
// Broadcast to all.
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
};
 
wss.on('connection', function connection(ws) {
    console.log("Got new connection");
    ws.on('message', function incoming(message) {
        console.log("Recieved message: " + message);
        let msgJson = {};
        let parsed = true;
        try{
            msgJson = JSON.parse(message);
        }
        catch(e){
            console.log("Incorrect data recieved: " + e);
            parsed = false;
        }

        if(parsed){
            if(msgJson.pw == 'sdbfo423pijs' && msgJson.action == 'new'){
                console.log("Sending broadcast!");
                // Broadcast to everyone else.
                wss.clients.forEach(function each(client) {
                    if (client != ws && client.readyState === WebSocket.OPEN) {
                        client.send(JSON.stringify(msgJson.data));
                    }
                });
            }
        }  
        

        //wss.broadcast(data);
    });
});