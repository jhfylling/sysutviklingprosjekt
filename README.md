# SysUtviklingProsjekt

Systemutvikling prosjekt høst 2018


commentWSS is a websocket server handling all comment communication.
All clients reading the same article will be updated with the latest comments through the websocket.

wss handles updating the DataStore when a new article with significance 1 is posted.

server is the restful API to serve most of the data and to recieve article uploads and edits.

Client is of course the react application.


Spesielle spennende features:
    1.Commmentsystemet er nesten det samme som en chat med forskjellige chatrooms
    2.Når man scroller i newsfeeden på fremsiden, lastes det nye artikler etterhvert som man skroller.
      Ved hjelp av IntersectionObserver, som er i ferd med å bli standarden nå.
    3.DataStore lagrer artikler slik at man i de fleste tilfeller ikke trenger å laste artikler mer enn en gang.
    4.DataStore får oppdaterte artikler gjennom websocket så snart de er lastet opp.
        Disse vises så i live newsfeeden øverst på siden og i newsfeeden på fremsiden.
    5.Live newsfeed øverst er skrevet med litt kreativ JS kode og er ikke optimal eller ryddig, men det var litt artig å lage den.
        Jeg ville ikke ha elementer som konstant beveger seg og valgte derfor denne løsningen. 