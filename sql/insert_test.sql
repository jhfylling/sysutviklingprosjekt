USE news_mock;

DELETE FROM comments;
DELETE FROM rating;
DELETE FROM news;
DELETE FROM categories;

ALTER TABLE news AUTO_INCREMENT = 1;
ALTER TABLE categories AUTO_INCREMENT = 1;
ALTER TABLE comments AUTO_INCREMENT = 1;
ALTER TABLE rating AUTO_INCREMENT = 1;

INSERT INTO categories (description) VALUES
  ('Sport'),
  ('Næringsliv'),
  ('Kjendiser'),
  ('Været'),
  ('utdanning');

INSERT INTO news (id, title, body, writer, upload_time, image, image_alt, category, significance, del)
  values (DEFAULT, 'Test title article one',"Article one test body", "Ola Nordmann", DEFAULT, 'statsbudsjett.jpg', 'Siv Jensen presenterer statsbudsjettet', 1, 1, 1),
  (DEFAULT, 'Voldsomt kaos', 'Fullt kaos i Buskerud KrF etter kun 38 prosent gikk imot Knut Arild Hareide', 'Ola Nordmann', DEFAULT, 'hareidekaos.jpg', 'Bilde av Knut Arild Hareide i trøbbel', 1, 1, 1),
  (DEFAULT, 'test artikkel tittel', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 1, 1, 1),
  (DEFAULT, 'test artikkel tittel', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 1, 2, 1),
  (DEFAULT, 'will be deleted test title', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 1, 1, 1),
  (DEFAULT, 'test artikkel tittel', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 1, 1, 1),
  (DEFAULT, 'test utdanning kategori', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 5, 1, 1),
  (DEFAULT, 'test prioritet 2', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 1, 2, 1),
  (DEFAULT, 'test artikkel tittel', 'test artikkel body', 'Ola Nordmann', DEFAULT, 'testtestbilde.jpg', 'testbilde', 1, 1, 1);


INSERT INTO comments (id, body, signature, upload_time, news_id) values
  (DEFAULT, 'Fake news!!!', 'Trump supporter', DEFAULT, 1);

INSERT INTO rating (id, value, news_id) VALUES
  (DEFAULT, 6, 1);
