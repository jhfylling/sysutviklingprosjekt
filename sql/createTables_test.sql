USE news_mock;

DROP TABLE IF EXISTS rating;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS news;
DROP TABLE IF EXISTS categories;

CREATE TABLE categories(
  id int NOT NULL AUTO_INCREMENT,
  description VARCHAR(64),
  CONSTRAINT pk PRIMARY KEY(id)
);

CREATE TABLE news(
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR (255),
  body TEXT NOT NULL,
  writer VARCHAR (255),
  upload_time TIMESTAMP,
  image VARCHAR (255),
  image_alt VARCHAR (255),
  category INT,
  significance BOOLEAN,
  del BOOLEAN,
  CONSTRAINT pk PRIMARY KEY(id),
  CONSTRAINT news_category FOREIGN KEY(category) REFERENCES categories(id)
);

CREATE TABLE comments(
  id INT NOT NULL AUTO_INCREMENT,
  body VARCHAR(512),
  signature VARCHAR(64),
  upload_time TIMESTAMP,
  news_id INT NOT NULL,
  CONSTRAINT pk PRIMARY KEY(id),
  CONSTRAINT comment_news FOREIGN KEY(news_id) REFERENCES news(id)
);

CREATE TABLE rating(
  id INT NOT NULL AUTO_INCREMENT,
  value INT NOT NULL,
  news_id INT NOT NULL,
  CONSTRAINT pk PRIMARY KEY(id),
  CONSTRAINT rating_news FOREIGN KEY(news_id) REFERENCES news(id)
);
