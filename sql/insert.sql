USE news_dev;

DELETE FROM comments;
DELETE FROM rating;
DELETE FROM news;
DELETE FROM categories;

ALTER TABLE news AUTO_INCREMENT = 1;
ALTER TABLE categories AUTO_INCREMENT = 1;
ALTER TABLE comments AUTO_INCREMENT = 1;
ALTER TABLE rating AUTO_INCREMENT = 1;

INSERT INTO categories (description) VALUES
  ('Sport'),
  ('Næringsliv'),
  ('Kjendiser'),
  ('Været'),
  ('utdanning');

INSERT INTO news (id, title, body, writer, upload_time, image, image_alt, category, significance, del)
  values (DEFAULT, 'I dag presenterte Siv Jensen statsbudsjettet for 2019',
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
    standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
    make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,'
    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem'
    Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'",
    "Ola Nordmann",
    DEFAULT, 'statsbudsjett.jpg', 'Siv Jensen presenterer statsbudsjettet', 1, 1, 1),
  (DEFAULT, 'Voldsomt kaos', 'Fullt kaos i Buskerud KrF etter kun 38 prosent gikk imot Knut Arild Hareide', 'Ola Nordmann', DEFAULT, 'hareidekaos.jpg', 'Bilde av Knut Arild Hareide i trøbbel', 1, 1, 1);


INSERT INTO comments (id, body, signature, upload_time, news_id) values
  (DEFAULT, 'Fake news!!!', 'Trump supporter', DEFAULT, 1);

INSERT INTO rating (id, value, news_id) VALUES
  (DEFAULT, 6, 1);
