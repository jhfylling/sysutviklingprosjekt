// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import  NewsCard  from '../src/components/NewsCard.js';
import { shallow, mount } from 'enzyme';

describe('Newscard tests', () => {

    const article = {
        id: 1,
        image: 'test.jpg',
        image_alt: 'alttext',
        title: 'testarticle',
        body: 'testbody',
    }
    const wrapper = shallow(<NewsCard story={article} />);
    it('Renders card', () => {
        expect(wrapper.find('.noStyle')).toBeDefined();
        expect(wrapper.find('.card-title')).toBeDefined();
    });

    it('Renders properties', () =>  {
        expect(wrapper.contains(<h5 className="card-title">testarticle</h5>)).toBeTruthy();
        expect(wrapper.contains(<p className="card-text">testbody</p>)).toBeTruthy();
        expect(wrapper.contains(<img className="card-img-top feed-card-img" src='/img/test.jpg' alt='alttext' />)).toBeTruthy();
    });
});
