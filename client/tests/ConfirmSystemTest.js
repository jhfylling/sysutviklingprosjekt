// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import  ConfirmSystem  from '../src/components/ConfirmSystem.js';
import { shallow, mount } from 'enzyme';

describe('ConfirmSystem tests', () => {

    function confirmTestFunction(){
        return 'testString';
    }

    const wrapper = shallow(
        <ConfirmSystem 
            alertClass='upload-btn' 
            buttonClass='btn upload teal'
            inner='Last opp' 
            action='Lagre' 
            onclick={confirmTestFunction} 
            body='Vil du lagre den nye artikkelen?' 
            title='Bekreft'
        />
    );

    it("Renders ConfirmSystem", () => {
        expect(wrapper.find('.upload-btn')).toBeDefined();
        expect(wrapper.find('.btn')).toBeDefined();
        expect(wrapper.find('.upload')).toBeDefined();
        expect(wrapper.find('.teal')).toBeDefined();
    });

    it("Renders correct button", () => {
        expect(wrapper.contains(<button type="button" className='btn upload teal' data-toggle="modal" data-target="#alertSystemModal">Last opp</button>)).toBeTruthy();
    });


    it("Passed function works correctly", () => {
        const instance = wrapper.instance();
        expect(instance.props.onclick()).toEqual('testString');
    });
});
