// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import  Newscase  from '../src/components/Newscase.js';
import { shallow, mount } from 'enzyme';

describe('Newscard tests', () => {

    const match = { params: { params: 1 } };
    const wrapper = shallow(<Newscase match={match}/>);
    it("Renders newscase", () => {
        expect(wrapper.find('.article-container')).toBeDefined();
        expect(wrapper.find('.news-title')).toBeDefined();
    });
});
