// @flow

import { sharedComponentData } from 'react-simplified';

class ArticleStore{
    articles = [];

    constructor(){
        this.initWebSocket();
    }

    initWebSocket(){
        this.ws = new WebSocket("ws://104.248.21.231:3001");

        this.ws.onopen = (event) => {
            this.onOpen(event);
        }

        this.ws.onmessage = (message) => {
            this.onMessage(message);
        }
    }

    onOpen(event: Event){
        console.log("ArticleStore connected to WS");
        // let message = {validator: 'safgn90aasdgfvvv!!z3c5xjy08', operation: 'load', id: this.props.id};
        // this.ws.send(JSON.stringify(message));
    }

    quick_Sort(origArray) {
        if (origArray.length <= 1) { 
            return origArray;
        } else {

            var left = [];
            var right = [];
            var newArray = [];
            var pivot = origArray.pop();
            var length = origArray.length;

            for (var i = 0; i < length; i++) {
                if (origArray[i].upload_time <= pivot.upload_time) {
                    left.push(origArray[i]);
                } else {
                    right.push(origArray[i]);
                }
            }

            return newArray.concat(this.quick_Sort(left), pivot, this.quick_Sort(right));
        }
    }

    onMessage(message: MessageEvent){
        console.log("Recieved new data from server");

        let newArray = [];
        newArray.push(JSON.parse(message.data));
        console.log(JSON.parse(message.data));
        for(let i: number = 0; i<this.articles.length; i++){
            console.log(this.articles[i]);
            newArray.push(this.articles[i]);
        }

        this.articles = newArray;

        //this.articles.push(JSON.parse(message.data));
        
        //Now proceed to sort articles
        //Note that if we simply add the new article to the front of a new array, we might have a sorted array.
        //That is unless, we updated the cache without sorting. Which is quite possible

        //this.articles = this.quick_sort(this.articles);
    }


    // var myArray = [3, 0, 2, 5, -1, 4, 1 ];

    // console.log("Original array: " + myArray);
    // var sortedArray = quick_Sort(myArray);
    // console.log("Sorted array: " + sortedArray);

    /**
    Checks if article is in store and resolves
    If not in store, adds it to store and resolve
     */
    getOneArticle(id: number){
        return new Promise((resolve, reject) => {
            let inStore: boolean = false;
            for(let i=0; i<this.articles.length; i++){
                if(this.articles[i].id == id){
                    console.log("DataStore: Article in store. Resolving");
                    inStore = true;
                    resolve(this.articles[i]);
                }
            }

            if(!inStore){
                console.log("DataStore: Articles not in store. Fetching");
                let promise = new Promise((resolve, reject) => {
                    this.fetchOneArticle(resolve, reject, id);
                });
                promise.then((response) => {
                    resolve(response);
                });
            }
        });
    }

    /**
        Returns latest articles in database. First checks if they are in store. Fetches if not.
     */
    getArticlesByCategory(limit: number, category: number){
        return new Promise((resolve, reject) => {
            if(limit <= this.getNumberOfArticlesInCategory(category)){
                console.log("DataStore: Articles in store");
                let articleList = [];

                for(let i: number; i<limit; i++){
                    if(this.articles[i].category === category){
                        articleList.push(this.articles[i]);
                    }
                }
                resolve(articleList);
            }
            else{
                console.log("DataStore: Articles not in store. Fetching");
                let promise = new Promise((resolve, reject) => {
                    this.fetchNewsByCategory(resolve, reject, limit, 0, category);
                });
                promise.then((response) => {
                    resolve(response);
                });
            }
        });
    }

    getNumberOfArticlesInCategory(category: number){
        let count: number = 0;
        for(let article in this.articles){
            if(article.category === category){
                count++;
            }
        }
        return count;
    }

    /**
        Returns latest articles in database. First checks if they are in store. Fetches if not.
     */
    getArticles(limit: number){
        return new Promise((resolve, reject) => {
            if(limit <= this.articles.length){
                console.log("DataStore: Articles in store");
                resolve(this.articles.slice(0, limit));
            }
            else{
                console.log("DataStore: Articles not in store. Fetching");
                let promise = new Promise((resolve, reject) => {
                    if(this.articles.length > 9){
                        this.fetchNews(resolve, reject, limit, this.articles.length);
                    }
                    else{
                        this.fetchNews(resolve, reject, limit, 0);
                    }
                });
                promise.then(() => {
                    resolve(this.articles.slice(0, limit));
                });
            }
        });
    }


    getArticlesOffset(limit: number, offset: number){
        return new Promise((resolve, reject) => {
            if(limit+offset <= this.articles.length){
                console.log("DataStore: Articles in store");
                resolve(this.articles.slice(offset, limit+offset));
            }
            else{
                console.log("DataStore: Articles not in store. Fetching");
                let promise = new Promise((resolve, reject) => {
                    this.fetchNews(resolve, reject, limit+offset, this.articles.length);
                });
                promise.then(() => {
                    resolve(this.articles.slice(offset, limit+offset));
                });
            }
        });
    }

    deleteArticle(id: number){
        return new Promise((resolve, reject) => {
            let tempArticles = [];
            this.articles.forEach((article) => {
                if(article.id !== id){
                    tempArticles.push(article);
                }
            });
            this.articles = tempArticles;

            let url = "story?id=" + id;
            fetch(url, {
                method: "DELETE",
            })
            .then(response => response.json())
            .catch(error => {
                console.error('Error:', error);
                reject(error);
            })
            .then(response => {
                resolve(response);
            });
        });
    }

    /**
     * Updates all loaded articles
     */
    updateStore(){
        //this.fetchNews(this.articles.length, 0);
    }

    /**
     * Fetches limited number of articles from server
     */
    fetchNews(resolve, reject, limit: number, offset: number){
        //Always fetch atleast 1 article
        if(limit < 1){
            limit = 1;
        }
        let url = "news?limit=" + limit + "&offset=" + offset + "&prio=1";
        fetch(url, {
            method: "GET",
        })
        .then(response => response.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            console.log("ArticleStore recieved articles");
            
            let i: number = offset;
            for(let row in response){
                //console.log(response[i]);
                this.articles[i] = response[row];
                i++;
            }
            resolve('loaded');
        });
    }


    /**
     * Fetches limited number of articles from server, in specific category
     */
    fetchNewsByCategory(resolve, reject, limit: number, offset: number, category: number){
        //Always fetch atleast 1 article
        if(limit < 1){
            limit = 1;
        }
        let url = "newsByCategory?limit=" + limit + "&offset=" + offset + "&prio=2&cat=" + category;
        fetch(url, {
            method: "GET",
        })
        .then(response => response.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            console.log("ArticleStore recieved articles");
            //first update cache
            for(let i: number; i<response.length; i++){
                let found: boolean = false;
                for(let j: number; j<this.articles.length; j++){
                    if(response[i].id === this.articles[j].id){
                        this.articles[j] = response[i];
                        found = true;
                    }
                }

                if(!found){
                    this.articles.push(response[i]);
                }
                else{
                    found = false;
                }
            }

            //now resolve response
            resolve(response);
        });
    }

    fetchOneArticle(resolve, reject, id: number){
        let url = "story?newsId=" + id;
        fetch(url, {
            method: "GET",
        })
        .then(response => response.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            console.log("ArticleStore recieved article");
            this.articles.push(response[0]);
            resolve(response[0]);
        });  
    }
}

export let articleStore = sharedComponentData(new ArticleStore());