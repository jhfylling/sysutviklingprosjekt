// @flow

import React from 'react';
import {Component} from 'react-simplified';
import { NavLink } from 'react-router-dom';
//import '../style/ConfirmSystem.css';
import {articleStore} from '../DataStore';

class ConfirmSystem extends Component{

    //<button type="button" className="btn btn-primary" data-toggle="modal" data-target=".alertSystemModal">Large modal</button>    
    render() {
        return (
            <div className={this.props.alertClass}>
                <button type="button" className={this.props.buttonClass} data-toggle="modal" data-target="#alertSystemModal">{this.props.inner}</button>

                <div className="modal fade" id="alertSystemModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalCenterTitle">{this.props.title}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {this.props.body}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Lukk</button>
                        <button type="button" onClick={this.props.onclick} className="btn btn-primary" data-dismiss="modal">{this.props.action}</button>
                    </div>
                    </div>
                </div>
                </div>

            </div>
        );
    }
}

export default ConfirmSystem;