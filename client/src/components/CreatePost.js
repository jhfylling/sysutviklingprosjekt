// @flow

import React from 'react';
import { Component } from 'react-simplified';
//import '../style/CreatePost.css';
import Quill from 'quill';
import FormData from 'form-data';
import ConfirmSystem from './ConfirmSystem';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

class CreatePost extends Component{

   state = {
       categories: []
   }

   quill: Quill;

render() {

    let cats = this.state.categories.map((category) => {
        return (
            <option value={category.id} key={category.id}>{category.description}</option>
        );
    });

    let tool =  <div className="CreatePost">
                    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet"></link>
                    <div className="container">
                        <form id="newArticleData" name="newArticleData">
                            <div className="form-group row">
                                <label htmlFor="lgFormGroupInput" className="col-sm-2 col-form-label col-form-label-lg">Tittel</label>
                                <div className="col-sm-10">
                                    <input type="text" name="title" className="form-control form-control-lg" id="lgFormGroupInput" placeholder="Tittel"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="smFormGroupInput" className="col-sm-2 col-form-label col-form-label-sm">Bilde</label>
                                <div className="col-sm-10">
                                <div className="input-group">
                                    <span className="input-group-btn">
                                        <span className="btn btn-default btn-file">
                                            <button type="button" className="btn btn-primary">Velg bilde</button>
                                            <input type="file" name="file" className="form-control form-control-sm" id="imgInp"/>
                                        </span>
                                    </span>
                                    <input type="text" className="form-control" readOnly/>
                                </div>
                                <img id='img-upload' alt='to upload'/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="smFormGroupInput" className="col-sm-2 col-form-label col-form-label-sm">Bildetekst</label>
                                <div className="col-sm-10">
                                    <input type="text" name="image_alt" className="form-control form-control-sm" id="smFormGroupInput" placeholder="Bildetekst"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="smFormGroupInput" className="col-sm-2 col-form-label col-form-label-sm">Forfatter</label>
                                <div className="col-sm-10">
                                    <input type="text" name="writer" className="form-control form-control-sm" id="smFormGroupInput" placeholder="Ola Nordmann"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="smFormGroupInput" className="col-sm-2 col-form-label col-form-label-sm">Kategori</label>
                                <div className="col-sm-10">
                                    <select name="category" className="form-control">
                                        {cats}
                                    </select>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="smFormGroupInput" className="col-sm-2 col-form-label col-form-label-sm">Viktighet</label>
                                <div className="col-sm-10">
                                    <select name="significance" className="form-control">
                                        <option value="1">1 (En)</option>
                                        <option value="2">2 (To)</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div id="editor"></div>
                        <ConfirmSystem 
                            alertClass='upload-btn' 
                            buttonClass='btn upload teal'
                            inner='Last opp' 
                            action='Lagre' 
                            onclick={this.uploadArticle} 
                            body='Vil du lagre den nye artikkelen?' 
                            title='Bekreft'
                        />
                    </div>
                    
                </div>;
                

    return (
        tool
    );
  }  

  mounted(){
    console.log("mounted");
    this.quill = new Quill('#editor', {
        theme: 'snow',
        placeholder: 'Artikkel'
    });

    const script = document.createElement("script");

    script.type="text/javascript";
    script.src = "./script/imagePreview.js";
    script.async = true;

    const docBody = document.body;

    if(docBody != null){
        docBody.appendChild(script);
    }


    this.fetchCategories();
  }

  uploadArticle(){

    console.log("Uploading article");

    let newArticleData = document.querySelector("#newArticleData");
    var formData = new FormData(newArticleData);
    
    let articleBody = this.quill.getText();
    formData.append('body', articleBody);
    
    //console.log(newArticleData);
    console.log(formData.values());

    fetch('/uploadArticle', {
      method: 'POST',
      body: formData
    })
    .then(response => response.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        console.log('Success:', JSON.stringify(response));
        history.push('/');
    });
  }

  fetchCategories(){
    let url = "categories";
    fetch(url, {
      method: "GET",
    })
    .then(response => response.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        let categories = response;
        this.setState({
            categories: categories
        });
    });
  }
}

export default CreatePost;