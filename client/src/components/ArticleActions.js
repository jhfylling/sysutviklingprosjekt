// @flow

import React from 'react';
import {Component} from 'react-simplified';
import { NavLink } from 'react-router-dom';
//import '../style/Newscase.css';
import {articleStore} from '../DataStore';
import ConfirmSystem from './ConfirmSystem';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

class ArticleActions extends Component{    
  render() {
    let confirmInner = <i className='fas fa-trash-alt' alt='Slett artikkel'></i>;
    return (
    <div className="article-action">
        <div className="article-rating">
            <button className="article-btn"><i className="fas fa-thumbs-up" alt="Tommel opp"></i></button>
            <button className="article-btn"><i className="fas fa-thumbs-down" alt="Tommel ned"></i></button>
            
        </div>
        <div className="article-admin">
            <NavLink to={"/editpost/" + this.props.id}>
                <button className="article-btn">
                <i className="far fa-edit" alt="Endre artikkel"></i>
                </button>
            </NavLink>
            <ConfirmSystem 
                alertClass='article-float-right' 
                buttonClass='article-btn'
                inner={confirmInner}
                action='Slett' 
                onclick={this.deleteArticle} 
                body='Vil du slette artikkelen?' 
                title='Bekreft'
            />
        </div>
    </div>
    );
  }

  deleteArticle(){
      console.log('Delete ' + this.props.id);
      articleStore.deleteArticle(this.props.id)
      .then((response) => {
          history.push('/');
      });
  }
}

export default ArticleActions;