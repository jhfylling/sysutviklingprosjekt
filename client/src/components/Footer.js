// @flow

import React from 'react';
import {Component} from 'react-simplified';
//import '../style/Footer.css';

class Footer extends Component {    
  render() {
    return (
<footer className="page-footer font-small special-color-dark darken-3">

    <div className="container">

      <div className="row">

        <div className="col-md-12 py-5">
          <div className="mb-5 flex-center">

            <a className="fb-ic footer-icon" href="#/">
              <i className="fab fa-facebook-f fa-2x"></i>
            </a>
            <a className="tw-ic footer-icon" href="#/">
              <i className="fab fa-twitter fa-2x"></i>
            </a>
            <a className="gplus-ic footer-icon" href="#/">
              <i className="fab fa-google-plus-g fa-2x"></i>
            </a>
            <a className="li-ic footer-icon" href="#/">
              <i className="fab fa-linkedin fa-2x"></i>
            </a>
            <a className="ins-ic footer-icon" href="#/">
              <i className="fab fa-instagram fa-2x"></i>
            </a>
            <a className="pin-ic footer-icon" href="#/">
              <i className="fab fa-pinterest fa-2x"></i>
            </a>
          </div>
        </div>

      </div>

    </div>

    <div className="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="#/">Johandev.no</a>
    </div>

  </footer>
    );
  }
}

export default Footer;