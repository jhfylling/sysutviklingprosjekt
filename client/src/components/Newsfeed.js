// @flow

import React from 'react';
import { Component } from 'react-simplified';
//import '../style/Newsfeed.css';
import NewsCard from './NewsCard';
import {articleStore} from '../DataStore';

class Newsfeed extends Component<{ match: { params: { category: number } } }> {
  target: HTMLElement;
  observedCounter = 0;
  loadedArticleCount = 0;
  ws: WebSocket;
  render() {
    
    //first create a two dimensional array with three newscases per row
    let newsMatrix = [];
    let j = 0;

    for(let i in articleStore.articles){
      if(Number(i)%3 === 0){
        j++;
        newsMatrix[j] = [];
      }
      newsMatrix[j].push(articleStore.articles[i]);
    }
    
    //then map newscases to JSX elements, using bootstrap rows and columns
    let articleCounter = 0;
    const newsGrid = newsMatrix.map((row, i) => {
        return(
                <div className="row" key={'row' + i}>
                  {newsMatrix[i].map((story) => {
                        return(
                          <div className="col-12 col-md-4" key={story.id} id={'article' + articleCounter++}>
                            <NewsCard story={story} />
                          </div>
                        );
                  })}
                  
                </div>
                );
    });

    return (
      <div className="Newsfeed" id="Newsfeed">
        {newsGrid}
      </div>
    );
  }

  mounted(){
    window.scrollTo(0,0);
    //load initial articles
    articleStore.getArticles(this.loadedArticleCount + 9).then((result) => {  
      this.loadedArticleCount+=9;
      this.forceUpdate();
      
      var options = {
        root: null,
        rootMargin: '0px',
        threshold: 0.5,
      }
      //Start observer
      var observer = new IntersectionObserver((() => {
        console.log("IntersectionTarget observed");

        if(this.observedCounter == 1){
          
          //load next articles and change observer target
          observer.unobserve(this.target);
          articleStore.getArticles(this.loadedArticleCount+9).then((result) => {
            this.loadedArticleCount+=9;
            this.forceUpdate();

            if(articleStore.articles.length >= this.loadedArticleCount){ //check if we have more articles
              console.log('ArticleLoader: ' + this.loadedArticleCount);
              this.target = document.querySelector('#article' + Number(this.loadedArticleCount-1));
              
              observer.observe(this.target);
              this.observedCounter = 0; 
            }
          });
        }
        else{
          this.observedCounter++;
        }
      }), options);

      this.target = document.querySelector('#article8');
      observer.observe(this.target);       

    });



  }  
}

export default Newsfeed;