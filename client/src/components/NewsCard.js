// @flow

import React from 'react';
import { Component } from 'react-simplified';
import { Link } from 'react-router-dom';

export default class NewsCard extends Component{
    render(){
        
        let story = this.props.story;
        let result = <div></div>;
        
        if(!story.body){
        result = 
            <Link className="noStyle" to={'/case/' + story.id}>
                <div className="card feedCard">
                    <img className="card-img-top feed-card-img" src={'/img/' + story.image} alt={story.image_alt} />
                    <div className="card-body">
                    <h5 className="card-title">{story.title}</h5>
                    </div>
                </div>
            </Link>;
        }
        else{
            result = 
                <Link className="noStyle" to={'/case/' + story.id}>
                    <div className="card feedCard">
                        <img className="card-img-top feed-card-img" src={'/img/' + story.image} alt={story.image_alt} />
                        <div className="card-body">
                        <h5 className="card-title">{story.title}</h5>
                        <p className="card-text">{story.body}</p>
                        <button type="button" className="btn teal">Les mer</button>
                        </div>
                    </div>
                </Link>;
        }

        return(
            result
        );
    }
}