// @flow

import React, { Component } from 'react';
//import '../style/Header.css';

type Props = {};

class Header extends Component<Props> {    
  render() {
    return (
      <h1 className="header">
        <img src="/img/heading.svg" alt="TrønderNews"/>
      </h1>
    );
  }
}

export default Header;