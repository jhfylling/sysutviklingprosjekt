// @flow

import React from 'react';
import { Component } from 'react-simplified';
//import '../style/Newscase.css';

class Comments extends Component {
    //state = {};
    ws: WebSocket;

    render() {
        let result = <div className="comment-container"></div>;

        let commentList = "";
        if(this.state && this.state[0]){
            commentList = 
            Object.values(this.state).map((comment) => {
                return(
                    <div className="comment" key={comment.id}>
                        <div className="comment-body">{comment.body}</div>
                        <div className="comment-author">- Av {comment.signature}</div>
                    </div>
                );
            });
        }

        result =
        <div className="comment-container">
            <h2 className="comment-container-title">Kommentarfelt</h2>
            {commentList}
            <div className="comment-new">
                <h4 className="h4">Skriv din mening</h4>
                <form>
                    <div className="form-group">
                        <label htmlFor="exampleFormControlTextarea1">Din kommentar</label>
                        <textarea className="form-control" id="newCommentBody" rows="3" placeholder="Skriv en kommentar her"></textarea>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleFormControlInput1">Navn</label>
                        <input type="text" className="form-control" id="newCommentAuthor" placeholder="Ola Nordmann" />
                    </div>
                    <div className="form-group">
                        <button type="button" onClick={this.newComment} className="btn teal" id="newCommentButton">Send</button>
                    </div>
                </form>
            </div>
        </div>
        

        return result;
    }

    newComment(){
        let newCommentBody: any = document.getElementById('newCommentBody');
        //if(newCommentBody && !(newCommentBody instanceof HTMLInputElement)) throw new Error('Expected an HTMLInputElement');

        let newCommentAuthor: HTMLInputElement | HTMLElement | null = document.querySelector('#newCommentAuthor');
        if(newCommentAuthor && !(newCommentAuthor instanceof HTMLInputElement)) throw new Error('Expected an HTMLInputElement');

        if(newCommentBody && newCommentAuthor){
            if(newCommentBody.value !== "" && newCommentAuthor !== ""){
                this.uploadComment(newCommentBody.value , newCommentAuthor.value);
            }
        }
    }

    mounted(){
        this.initWebSocket();
    }

    initWebSocket(){
        this.ws = new WebSocket("ws://104.248.21.231:3002");

        this.ws.onopen = (event) => {
            this.onOpen(event);
        }

        this.ws.onmessage = (message) => {
            this.onMessage(message);
        }
    }

    onOpen(event: Event){
        console.log("Connected to comment server");
        let message = {validator: 'safgn90aasdgfvvv!!z3c5xjy08', operation: 'load', id: this.props.id};
        this.ws.send(JSON.stringify(message));
    }

    onMessage(message: MessageEvent){
        console.log("Recieved data from server");
        this.setState(JSON.parse(message.data));
    }

    uploadComment(comment: string, author: string){
        console.log("Comments: uploading comment from " + author);
        let message = {validator: "safgn90aasdgfvvv!!z3c5xjy08", operation: 'save', id: this.props.id, data: {comment: comment, author: author}};
        this.ws.send(JSON.stringify(message));
    }
}

export default Comments;