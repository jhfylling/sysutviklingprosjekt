// @flow

import React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';
//import '../style/Menu.css';

class Menu extends Component {

  state = {
    categories: []
  }

  render() {
    let cats = "";
    if(this.state.categories){
      cats = this.state.categories.map((category) => {
          return (
              <li className="nav-item" key={category.id}>
                <NavLink className="nav-link category" to={'/category/' + category.id}>{category.description}</NavLink>
              </li>
          );
      });
    }

    return (
      <nav className="navbar navbar-expand-lg navbar-dark special-color-dark">
        <a className="navbar-brand" href="#/">
          <img src="/img/header.svg" className="menu-icon" alt="TrønderNews"/>
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {cats}
          </ul>
          <NavLink className="btn teal my-2 my-sm-0" to="/newpost">Nytt innlegg</NavLink>
        </div>
      </nav>
    );
  }

  mounted(){
    this.fetchCategories();
  }

  fetchCategories(){
    let url = "categories";
    fetch(url, {
      method: "GET",
    })
    .then(response => response.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        let categories = response;
        this.setState({
            categories: categories
        });
    });
  }
}

export default Menu;