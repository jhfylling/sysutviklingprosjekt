// @flow

import React from 'react';
import { Component } from 'react-simplified';
//import '../style/Newsfeed.css';
import NewsCard from './NewsCard';
import {articleStore} from '../DataStore';

class CategoryFeed extends Component<{ match: { params: { category: number } } }> {
  target: HTMLElement;
  observedCounter = 0;
  loadedArticleCount = 0;
  articleList = [];

  render() {
    //first create a two dimensional array with three newscases per row
    let newsMatrix = [];
    let j = 0;

    console.log(this.articleList);

    for(let i in this.articleList){
      if(Number(i)%3 === 0){
        j++;
        newsMatrix[j] = [];
      }
      newsMatrix[j].push(this.articleList[i]);
    }
    
    //then map newscases to JSX elements, using bootstrap rows and columns
    let articleCounter = 0;
    const newsGrid = newsMatrix.map((row, i) => {
        return(
                <div className="row" key={'row' + i}>
                  {newsMatrix[i].map((story) => {
                        return(
                          <div className="col-12 col-md-4" key={story.id} id={'article' + articleCounter++}>
                            <NewsCard story={story} />
                          </div>
                        );
                  })}
                  
                </div>
                );
    });

    return (
      <div className="Newsfeed" id="Newsfeed">
        {newsGrid}
      </div>
    );
  }

  mounted(){
    //load initial articles

    console.log('Category: ', this.props.match.params.category);
    articleStore.getArticlesByCategory(20, this.props.match.params.category)
    .then((response) => {
      this.articleList = response;
      //this.forceUpdate();
    });
    


  }  
}

export default CategoryFeed;