// @flow

import React from 'react';
import {Component} from 'react-simplified';
import {Link} from 'react-router-dom';
//import '../style/NewsCarousel.css';
import {articleStore} from '../DataStore';
import Time from 'react-time-format';


export default class NewsCarousel extends Component{

    stage = 0;  //Swaps between stage 0, 1 and 2. (3 groups of articles. 9 Articles displayed in total)

    state = { stories: []};
    interval = null;

    render(){
        let stories;
        if(this.state){
            stories = this.state.stories.map((story) => {
                return <CarouselCard story={story} key={story.id} />
            });
        }

        return(
            <div className='carousel'>
                <div className='slide'>
                    {stories}
                </div>
            </div>
        );
    }

    mounted(){
        this.fetchNews();  
        //this.initWebSocket();

        //this.interval = setInterval(this.swapSlide, 6000 * 5);
        this.interval = setInterval(this.getThreeArticles, 1000 * 60);
    }

    beforeUnmount() {
        if (this.interval) clearInterval(this.interval);
    }

    slideOut(resolve: any, reject: any){
        let slide = document.querySelector(".slide");
        if(slide != null){
            let width = Math.ceil(slide.clientWidth / 50) * 50;

            let pos = 0;
            let interval = setInterval(moveSlide, 10);

            function moveSlide(){
                if(pos === width){
                    clearInterval(interval);
                    resolve('out');
                }
                else{
                    pos+=50;
                    
                        slide.style.left = pos + 'px';
                    
                }
            }
        }        
    }

    slideIn(){
        let slide = document.querySelector(".slide");
        if(slide != null){
            let width = Math.ceil(slide.clientWidth / 50) * 50;
            
            let pos = width;
            let interval = setInterval(moveSlide, 10);

            function moveSlide(){
                if(pos === 0){
                    clearInterval(interval);
                }
                else{
                    pos-=50;
                    slide.style.left = pos + 'px';
                }
            }
        }    
    }

    swapSlide(){
        let promise1 = new Promise(this.slideOut);
        promise1.then(this.slideIn);
    }

    swapSlideData(stories: Object){
        let promise1 = new Promise(this.slideOut);
        promise1.then(() => {
            this.setState({stories});
            this.slideIn();
        });
    }

    initWebSocket(){
        this.ws = new WebSocket("ws://104.248.21.231:3001");

        this.ws.onopen = (event) => {
            this.onOpen(event);
        }

        this.ws.onmessage = (message) => {
            this.onMessage(message);
        }
    }

    onOpen(event: Event){
        console.log("Connected to WS server");
    }

    onMessage(message: MessageEvent){
        console.log("Recieved broadcast!");
        let tempState: any = this.state;
        tempState.news.push(JSON.parse(String(message.data)));
        this.setState(tempState);
    }

    sendMessage(message: string){
        this.ws.send(message);
    }

    getThreeArticles(){
        let offset = this.stage * 3;   //set offset in order to swap between which articles we want
        let limit = 3;  //Only display 3 articles at once

        console.log("NewsCarousel: offset=" + offset);

        articleStore.getArticlesOffset(limit, offset).then((result) => {
            console.log("NewsCarousel:" + result[0].title, result[1].title, result[2].title);
            this.swapSlideData(result);
        });

        if(this.stage === 2){
            this.stage = 0;
        }
        else{
            this.stage++;
        }
    }


    /**
     * Fetches all news from server and sets state object
     */
    fetchNews(){
        let url = "news?prio=1&cat=0&limit=3&offset=0";
        fetch(url, {
            method: "GET",
        })
        .then(response => response.json())
        .catch(error => console.error('Error:', error))
        .then(stories => {
            this.setState({stories});
        });
    }
}

class CarouselCard extends Component{
    render(){
        
        
        let story = this.props.story;

        /*
        let date = new Date(story.upload_time);
        let days = ['mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag', 'lørdag', 'søndag'];

        let minutes: string = '0' + String(date.getMinutes()));
        if(date.getMinutes() >= 10){
            minutes: string = String(date.getMinutes());
        }
        let time = days[date.getDay()] + ' ' + date.getHours() + ':' + date.getMinutes();
        */
        let time = new Date(story.upload_time);
        return(
            <Link className="noStyle" to={'/case/' + story.id}>
                <div className="carousel-card">
                    <h5 className="card-title">{story.title}</h5>
                    <h5 className="card-title"><Time value={time} format="YYYY/MM/DD hh:mm" /></h5>
                </div>
            </Link>
        );
    }
}