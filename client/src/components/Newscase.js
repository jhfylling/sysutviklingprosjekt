// @flow

import React from 'react';
import { Component } from 'react-simplified';
import {articleStore} from '../DataStore';
import Comments from './Comments';
import ArticleActions from './ArticleActions';
import Time from 'react-time-format';

class Newscase extends Component<{ match: { params: { id: number } } }> {
  
  state = {};
  
  render() {
    window.scrollTo(0,0);
    let newsCase = <div></div>;

    if(this.state.title){

      let time = new Date(this.state.upload_time);

      newsCase = 
          <div className="article-container">
            <img className="article-image" src={'/img/' + this.state.image} alt={this.state.image_alt}/>
            <div className="image-text">{this.state.image_alt}</div>
            <h1 className="news-title">{this.state.title}</h1>
            <div className="article-writer">Skrevet av {this.state.writer} | <Time value={time} format="YYYY/MM/DD hh:mm" /></div>
            <div className="news-body">{this.state.body}</div>
            <ArticleActions id={this.state.id} />
            <Comments id={this.state.id} />
          </div>;
    }
    return (
      newsCase
    );
  }

  mounted(){
    console.log("mounted");
    let id = this.props.match.params.id;
    articleStore.getOneArticle(id).then((response) => {
      this.setState(response);
    });
  }
}

export default Newscase;