// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { HashRouter, Route } from 'react-router-dom';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();


const root = document.getElementById('root');

if(root != null){
    ReactDOM.render(
        <HashRouter>
            <Route path="/" component={App}/>
        </HashRouter>, 
        root
    );
}
registerServiceWorker();
