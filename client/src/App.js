// @flow

import React from 'react';
import { Component } from 'react-simplified';
import Newsfeed from './components/Newsfeed';
import Menu from './components/Menu';
import Newscase from './components/Newscase';
import CreatePost from './components/CreatePost';
import NewsCarousel from './components/NewsCarousel';
import Footer from './components/Footer';
import EditPost from './components/EditPost';
import CategoryFeed from './components/CategoryFeed';
import { Route, Switch } from 'react-router-dom';
import './style/Newscase.css';
import './style/Menu.css';
import './style/Newsfeed.css';
import './style/Footer.css';
import './style/CreatePost.css';
import './style/ConfirmSystem.css';
import './style/Header.css';
import './style/NewsCarousel.css';

class App extends Component {
  render() {
    return(
      <div className="App">
      <Menu />
      <NewsCarousel />
      <Switch>
        <Route exact path="/" component={Newsfeed} />    
        <Route exact path="/category/:category" component={CategoryFeed} />    
        <Route exact path="/case/:id" component={Newscase} />    
        <Route exact path="/newpost" component={CreatePost} />    
        <Route exact path="/editpost/:id" component={EditPost} />    
      </Switch>
      <Footer />
      </div>
    )
  }
}

export default App;
