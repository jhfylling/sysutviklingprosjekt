// flow-typed signature: b5582635995233b60d961580944d1f9f
// flow-typed version: <<STUB>>/express-formidable_v^1.2.0/flow_v0.86.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'express-formidable'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'express-formidable' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'express-formidable/lib/middleware' {
  declare module.exports: any;
}

// Filename aliases
declare module 'express-formidable/lib/middleware.js' {
  declare module.exports: $Exports<'express-formidable/lib/middleware'>;
}
