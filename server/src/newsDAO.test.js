var mysql = require("mysql");
var fs = require("fs");

const NewsDAO = require('./newsDAO.js');

// Johandev MySQL server
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "johandev.no",
  user: "newsadmin",
  password: "ciBJ8o0IxgT8YGJg",
  database: "news_mock",
  debug: false,
  multipleStatements: true
});

var newsDAO = new NewsDAO(pool);

afterAll(() => {
  pool.end((err) => {
    if(err){
      console.log(err);
    }
    else{
      console.log("Disconnected from database\nTest completed");
    }
  });
});

beforeAll(done => {
    runSQLfile("../sql/createTables_test.sql", pool, () => {
        runSQLfile("../sql/insert_test.sql", pool, done);
    });
});

test("get one article from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].title).toBe("Test title article one");
    done();
  }

  newsDAO.getOneStory(1, callback);
});

test("get news with params from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(4);  //test that the limit works
    expect(data[0].title).toBe("Test title article one"); //test that we get any articles at all
    expect(data[3].title).toBe("test artikkel tittel"); //test that we get articles with prio 2 as well as 1

    done();
  }

  let params = {prio: 2, limit: 4, offset: 0}
  newsDAO.getNews(params, callback);
});


test("getNewsByCategory", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(1);
    expect(data[0].title).toBe("test utdanning kategori"); //test that we get the only article in the DB with category 5

    done();
  }

  let params = {cat: 5, limit: 4, offset: 0}
  newsDAO.getNewsByCategory(params, callback);
});

test("getComments", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(1);
    expect(data[0].body).toBe("Fake news!!!");

    done();
  }

  newsDAO.getComments(1, callback);
});

test("getRating", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(1);
    expect(data[0].value).toBe(6);  //check that the returned rating value from news article 1 is correct

    done();
  }
  
  newsDAO.getRating(1, callback);
});

test("getCategories", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.length).toBe(5);
    expect(data[0].description).toBe("Sport");

    done();
  }
  
  newsDAO.getCategories(callback);
});


test("saveArticle", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.affectedRows).toBe(1);
    expect(data.serverStatus).toBe(2);
    expect(data.warningCount).toBe(0);

    newsDAO.getOneStory(data.insertId, (status, data) => {
      expect(data.length).toBe(1);
      expect(data[0].title).toBe("12390asdklcfutcfut123bvdfhsfgh0990234sdfvb");
      done();
    });
    //expect(data[0].description).toBe("Sport");

  }
  
  let data = {title: "12390asdklcfutcfut123bvdfhsfgh0990234sdfvb", body: "test insert body", image: "testimage.jpg", image_alt: "test image", category: 1, significance: 1};
  newsDAO.saveArticle(data, callback);
});


test("saveComment", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.affectedRows).toBe(1);
    expect(data.serverStatus).toBe(2);
    expect(data.warningCount).toBe(0);

    newsDAO.getComments(2, (status, data) => {
      expect(data[0].body).toBe("12390asdklcfutcfut123bvdfhsfgh0990234sdfvb");
      expect(data[0].signature).toBe("09ufeghpioi12350a9dcvadckkladsfg0pnplk");
      done();
    });
    //expect(data[0].description).toBe("Sport");

  }
  
  let data = {body: "12390asdklcfutcfut123bvdfhsfgh0990234sdfvb", signature: "09ufeghpioi12350a9dcvadckkladsfg0pnplk", news_id: 2};
  newsDAO.saveComment(data, callback);
});

test("Test disableArticle", done => {
  function callback(status, data) {
    console.log(
      "Test disableArticle callback: status=" + status + ", data=" + JSON.stringify(data)
    );

    expect(data.affectedRows).toBe(1);
    expect(data.serverStatus).toBe(2);
    expect(data.warningCount).toBe(0);

    newsDAO.getOneStory(5, (status, data) => {
      expect(data.length).toBe(0);
      done();
    });

  }
  
  let id = 5; 
  newsDAO.disableArticle(id, callback);
});





var runSQLfile = ((filename, pool, done) => {
  console.log("runsqlfile: reading file " + filename);
  let sql = fs.readFileSync(filename, "utf8");
  pool.getConnection((err, connection) => {
    if (err || connection === undefined) {
      console.log("runsqlfile: error connecting");
      done();
    } else {
      console.log("runsqlfile: connected");
      connection.query(sql, (err, rows) => {
        connection.release();
        if (err) {
          console.log(err);
          done();
        } else {
          console.log("runsqlfile: run ok");
          done();
        }
      });
    }
  });
});
