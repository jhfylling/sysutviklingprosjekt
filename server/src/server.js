// @flow
/* Online newspaper! Taking the paper out of the newssa */

var express = require('express');
var formidableMiddleware = require('express-formidable');
var fs = require('fs');
var WebSocket = require('ws');
var app = express();
var mysql = require("mysql");

const NewsDAO = require('./newsDAO.js');


let pool = mysql.createPool({
  connectionLimit: 2,
  host: "johandev.no",
  user: "newsadmin",
  password: "ciBJ8o0IxgT8YGJg",
  database: "news_dev",
  debug: false
});

let newsDAO = new NewsDAO(pool);

import type {
  $Request,
  $Response,
  NextFunction,
  Middleware,
} from 'express';

//app.use(express.static('public'));
app.use(formidableMiddleware());

app.get('/hei', (req, res: $Response) => {
  res.send("Hei tilbake!");
});

app.get('/allnews', (req, res: $Response) => {
  newsDAO.getAll((status, data) =>{
    res.status(status);
    res.json(data);
  });
});

app.get('/news', (req, res: $Response) => {
  console.log("recieved request");
  let q = req.query;
  let params = {prio: q.prio, limit: q.limit, offset: q.offset}
  newsDAO.getNews(params, (status, data) =>{
    res.status(status);
    res.json(data);
  });
});

app.get('/newsByCategory', (req, res: $Response) => {
  let q = req.query;
  let params = {cat: q.cat, limit: q.limit, offset: q.offset}
  newsDAO.getNewsByCategory(params, (status, data) =>{
    res.status(status);
    res.json(data);
  });
});


app.get('/story', (req, res: $Response) => {
  newsDAO.getOneStory(req.query.newsId, (status, data) =>{
    res.status(status);
    res.json(data);
  });
});


/*
app.get('/rating', (req, res: $Response) => {
  newsDAO.getRating(req.query.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.put('/rating', (req, res: $Response) => {
  let value = 0;
  if(req.query.value >= 1){
    value = 1;
  }
  else if(req.query.value <= -1){
    value = -1;
  }
  if(value !== 0){
    newsDAO.addRating(req.query.id, value)) => {
      res.status(status);
      res.json(data);
    });
  }
});

*/

app.delete('/story', (req, res: $Response) => {
  newsDAO.disableArticle(req.query.id, (status, data) =>{
    res.status(status);
    res.json(data);
  });
});

app.post('/editArticle', (req, res: $Response) => {
  console.log("storing data in db");

  let statusr = 200;
  let datar = {success: true};

  let dbData = req.fields;
  dbData.image = 'storyimg/' + req.files.file.name;

  newsDAO.updateArticle(dbData, (status, data) => {
    statusr = status;
    datar = data;
  });

  console.log("moving file");
  if(req.files.file){
    let oldpath = req.files.file.path;
    let newpath = '/home/web/sysutviklingprosjekt/client/public/img/storyimg/' + req.files.file.name;
    fs.rename(oldpath, newpath, (err) => {
      if(err) throw err;
      console.log("Filed uploaded and moved");
    });
  }

  res.status(statusr);
  res.json(datar);

});

app.get('/categories', (req, res: $Response) => {
  newsDAO.getCategories((status, data) => {
    res.status(status);
    res.json(data);
  })
});

app.post('/uploadArticle', (req, res: $Response) => {
  console.log("storing data in db");

  let statusr = 200;
  let datar = {success: true};

  let dbData = req.fields;
  dbData.image = 'storyimg/' + req.files.file.name;

  console.log(dbData);
  
  newsDAO.saveArticle(dbData, (status, data) => {
    console.log('data: ', data);
    let articleId = data.insertId;
    statusr = status;
    datar = data;
    //If the new article is of priority 1, update every client's scrolling feed
    //by sending a message to our WS server
    if(dbData.significance == 1 && statusr == 200){
      newsDAO.getOneStory(articleId, (status2, data2) =>{
        if(status2 === 200){
          console.log(data2);
          let ws = new WebSocket("ws://104.248.21.231:3001");
          ws.on('open', () => {
            let message = {
              pw: 'sdbfo423pijs',
              action: 'new',
              data: data2[0],
            }
            ws.send(JSON.stringify(message));
          });
        }
        else{
          console.log("UploadArtilce: Could not load data from database");
        }
      });
    }


  });

  console.log("moving file");
  if(req.files.file){
    let oldpath = req.files.file.path;
    let newpath = '/home/web/sysutviklingprosjekt/client/public/img/storyimg/' + req.files.file.name;
    fs.rename(oldpath, newpath, (err) => {
      if(err) throw err;
      console.log("Filed uploaded and moved");
    });
  }

  

  res.status(statusr);
  res.json(datar);

});

var server = app.listen(8080, function(){
  if(server != null){
    var host = server.address().address;
    var port = server.address().port;
    console.log("Server listening on http://%s:%s", host, port);
  }
  else{
    console.log("Server could not start");
  }
});
