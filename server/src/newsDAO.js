// @flow

const Db = require("./db.js");

module.exports = class NewsDAO extends Db{
  getAll(callback: any){
    super.query("SELECT * FROM news WHERE del = 1 ORDER BY upload_time DESC;", [], callback);
  }

  getNews(params: Object, callback: any){
    let p = params;

    super.query(
      "SELECT news.*, rating.value AS rating FROM news " + 
      "LEFT JOIN rating ON news.id = rating.news_id " + 
      "WHERE news.significance <= ? AND news.del = 1 " +
      "ORDER BY news.upload_time DESC " +
      "LIMIT ? OFFSET ?;",
      [Number(p.prio), Number(p.limit), Number(p.offset)],
      callback,
    );
  }

  getNewsByCategory(params: Object, callback: any){
    let p = params;
    //console.log(p);

    super.query(
      "SELECT news.*, SUM(rating.value) AS rating FROM news LEFT JOIN rating ON news.id = rating.news_id WHERE news.category = ? AND del = 1 GROUP BY news.id ORDER BY news.upload_time DESC LIMIT ? OFFSET ?;",
      [Number(p.cat), Number(p.limit), Number(p.offset)],
      callback,
    );
  }

  getOneStory(id: number, callback: any){
    super.query(
      "SELECT news.*, rating.value AS rating FROM news LEFT JOIN rating ON news.id = rating.news_id WHERE news.id = ? AND del = 1;",
      [id],
      callback
    );
  }

  getComments(news_id: number, callback: any){
    super.query("SELECT * FROM comments WHERE news_id = ?;", [news_id], callback);
  }

  getRating(news_id: number, callback: any){
    super.query("SELECT value FROM rating WHERE news_id = ?;", [news_id], callback);
  }

  getCategories(callback: any){
    super.query("SELECT * FROM categories;", [], callback);
  }

  saveArticle(data: Object, callback: any){
    super.query("INSERT INTO news (id, title, body, upload_time, image, image_alt, category, significance, del) VALUES (DEFAULT, ?, ?, DEFAULT, ?, ?, ?, ?, 1);",
    [data.title, data.body, data.image, data.image_alt, data.category, data.significance],
    callback);
  }

  updateArticle(data: Object, callback: any){
    super.query("UPDATE news SET title=?, body=?, image=?, image_alt=?, upload_time=upload_time, category=?, significance=? WHERE id=?;",
    [data.title, data.body, data.image, data.image_alt, data.category, data.significance, data.id],
    callback);
  }

  saveComment(data: Object, callback: any){
    super.query("INSERT INTO comments (id, body, signature, upload_time, news_id) VALUES (DEFAULT, ?, ?, DEFAULT, ?);",
    [data.body, data.signature, data.news_id],
    callback);
  }

  disableArticle(id: number, callback: any){
    super.query("UPDATE news SET del = 0 WHERE id = ?;",
    [id],
    callback);
  }
};
